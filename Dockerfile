FROM apteno/alpine-jq

RUN apk add --no-cache bash

COPY backup.sh /bin/

WORKDIR /data

# run without privileges
ENV USER=app
ENV UID=1000
ENV GID=1000


RUN addgroup --gid "$GID" "$USER" && adduser --disabled-password --no-create-home --gecos "" --home "$(pwd)" --ingroup "$USER" --uid "$UID" "$USER"

USER app

CMD [ "backup.sh" ]
