#!/usr/bin/env bash

[[ $DEBUG ]] && set -ex

if [[ -n $GRAFANA_KEY ]]; then
  echo "Getting grafana key from Env Var"
  KEY=$GRAFANA_KEY
else
  KEY=$(</run/secrets/grafana-key)
  echo "Getting grafana key from secret $GRAFANA_SECRET_NAME"
fi

HOST=${GRAFANA_HOST:-'https://grafana:3000'}
backup_dir=${BACKUP_DIR:-/data}

[[ -z $KEY ]] && echo "Missing grafana key" && exit 1

if [ ! -d $backup_dir ] ; then
    mkdir -p $backup_dir
fi

dash_uids=$(curl --silent -k -H "Authorization: Bearer $KEY" "$HOST/api/search?query=" | jq -r '.[].uid')
for dash in $dash_uids; do
  echo "==> Backing up dashboard uid: '$dash'"
  curl --silent -k -H "Authorization: Bearer $KEY" $HOST/api/dashboards/uid/$dash | sed 's/"id":[0-9]\+,/"id":null,/' | sed 's/\(.*\)}/\1,"overwrite": true}/' > ${backup_dir}/${dash}.json
done

echo "==> Finished successfully"
ls -h $backup_dir

